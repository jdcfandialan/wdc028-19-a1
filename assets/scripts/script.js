let sum = 0;
let num = 10;

for(let i = 0;i <= num;i++){
	sum += i;

	console.log(`+${i} = ${sum}`);
}

for(let i = 0;i <= 15;i++){
	if(i % 2 === 0){
		console.log(`${i} is even`);
	}

	else console.log(`${i} is odd`);
}

let product = "   "
for(let i = 0;i <=10;i++){
	for(let j = 0;j <= 10;j++){
		if(i === 0 && j > 0){
			product += `     [` + j + `] `;
		}

		else if(j === 0 && i > 0){
			product += `[` + i + `] `;
		}

		else if(i > 0 && j > 0){
			product += "     " + (i*j) + "  ";
		}
	}
	product += '\n';
}

console.log(product);

let pattern = "";
let size = 7;
for(let i = 0;i <= size;i++){
	for(let j = 0;j <= size;j++){
		if(i > j){
			pattern += "#";
		}

		else continue;
	}
	pattern += '\n';
}

console.log(pattern);